# alpine-scepg

#### [alpine-x64-scepg](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-scepg/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpinex64/alpine-x64-scepg/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpinex64/alpine-x64-scepg/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpinex64/alpine-x64-scepg/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinex64/alpine-x64-scepg)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinex64/alpine-x64-scepg)
#### [alpine-aarch64-scepg](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-scepg/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpineaarch64/alpine-aarch64-scepg/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpineaarch64/alpine-aarch64-scepg/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpineaarch64/alpine-aarch64-scepg/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpineaarch64/alpine-aarch64-scepg)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpineaarch64/alpine-aarch64-scepg)
#### [alpine-armhf-scepg](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-scepg/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpinearmhf/alpine-armhf-scepg/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpinearmhf/alpine-armhf-scepg/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpinearmhf/alpine-armhf-scepg/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinearmhf/alpine-armhf-scepg)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinearmhf/alpine-armhf-scepg)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64,armhf
* Appplication : [scepg](https://github.com/forumi0721alpinex64/scepg)
    - Customizable Korean EPG generator



----------------------------------------
#### Run

```sh
docker run -d \
           -v /repo:/repo \
           -e RUN_USER_NAME=scepg \
           -e RUN_USER_UID=1000 \
           -e RUN_USER_GID=100 \
           -e SCEPG_OUTPUT_SOCKET=tcp:localhost:9983 \
           -e CRON_SCHEDULE="0 */4 * * *" \
           forumi0721alpine[ARCH]/alpine-[ARCH]-scepg:latest
```



----------------------------------------
#### Usage

* Nothing to do. Just set config and schedule.
    - If you use *Tvheadend*, you need soket relay.
    - If you want to use complex setting, you need to create `tvchannel.conf` and add `-v tvchannel.conf:/etc/tvchannel.conf` to docker option.
    - If you want to set schedule, you need to create `schedule` and add `-v schedule:/etc/schedule` to docker option.



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| /repo              | Persistent data                                  |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| RUN_USER_NAME      | Application execute username (default : scepg)   |
| RUN_USER_UID       | Application execute user uid (default : 1000)    |
| RUN_USER_GID       | Application execute user uid (default : 100)     |
| SCEPG_OUTPUT_SOCKET| Output socket (ex. tcp:<ip>:<port> or <path>/xmltv.sock) |
| CRON_SCHEDULE      | EPG grab schedule (default : 0 */4 * * *)        |

